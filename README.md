# File Transfer pricing component

![Design preview for the File Transfer pricing component coding challenge](./design/desktop-preview.jpg)


**To do this challenge, you need a basic understanding of HTML, CSS and JavaScript.**

## The challenge

Your challenge is to build out this pricing component and get it looking as close to the design as possible.

You can use any tools you like to help you complete the challenge. So if you've got something you'd like to practice, feel free to give it a go.

Your users should be able to:

- View the optimal layout for the component depending on their device's screen size
- Control the toggle with both their mouse/trackpad and their keyboard
- **Bonus**: Complete the challenge with just HTML and CSS


## Where to find everything

Your task is to build out the project to the designs inside the `/design` folder. You will find both a mobile and a desktop version of the design to work to, as well as a design for the active states.

The designs are in JPG static format. This will mean that you'll need to use your best judgment for styles such as `font-size`, `padding` and `margin`. This should help train your eye to perceive differences in spacings and sizes.

You will find all the required assets in the `/images` folder. The assets are already optimized.

There is also a `style-guide.md` file, which contains the information you'll need, such as color palette and fonts.

## Building your project

Feel free to use any workflow that you feel comfortable with. Below is a suggested process, but do not feel like you need to follow these steps:

1. Fork the repository.
2. Configure your repository to publish your code to a URL. This will also be useful if you need some help during a challenge as you can share the URL for your project with your repo URL. There are a number of ways to do this, but we recommend using [ZEIT Now](http://bit.ly/fem-zeit). We've got more information about deploying your project with ZEIT below.
3. Look through the designs to start planning out how you'll tackle the project. This step is crucial to help you think ahead for CSS classes that you could create to make reusable styles.
4. Before adding any styles, structure your content with HTML. Writing your HTML first can help focus your attention on creating well-structured content.
5. Write out the base styles for your project, including general content styles, such as `font-family` and `font-size`.
6. Start adding styles to the top of the page and work down. Only move on to the next section once you're happy you've completed the area you're working on.
7. If you'd like to try making your project fully responsive, we'd recommend checking out [Sizzy](http://bit.ly/fem-sizzy). It's a great browser that makes it easy to view your site across multiple devices.

## Deploying your project

As mentioned above, there are a number of ways to host your project for free. We recommend using [ZEIT Now](http://bit.ly/fem-zeit) as it's an amazing service and extremely simple to get set up with. If you'd like to use ZEIT, here are some steps to follow to get started:

1. [Sign up to ZEIT Now](http://bit.ly/fem-zeit-signup) and go through the onboarding flow, ensuring your GitHub account is connected by using their [ZEIT Now for GitHub](https://zeit.co/docs/v2/git-integrations/zeit-now-for-github) integration.
2. Connect your project to ZEIT Now from the ["Create a new project" page](https://zeit.co/new), using the "New Project From GitHub" button and selecting the project you want to deploy.
3. Once connected, every time you `git push`, ZEIT Now will create a new [deployment](https://zeit.co/docs/v2/platform/deployments) and the deployment URL will be shown on your [ZEIT Dashboard](https://zeit.co/dashboard). You will also receive an email for each deployment with the URL.


**Have fun building!** 🚀












*{
    margin:0;
    padding: 0;
    font-family: 'Times New Roman', Times, serif;
    font-weight: 500;
    font-size: 18px;
    /* user-select: none; */
    /* overflow: hidden; */

}
.menu {
    display: flex;
   flex-direction: row;
   justify-content: space-around;
   align-items: center;
   padding: 20px 0;
}

.menu nav ul{
    list-style-type: none;
    display: flex;
    gap: 15px;
    margin: 0;
    padding: 0;
    
}
.menu nav a{
    text-decoration: none;
    font-size: 18px;
    color: black;
}

.menu nav a:hover{
    font-size:24px;
    background-color: aqua;
    color: rgb(131, 31, 224);
    width: 30px;
}

.menu nav ul li{
    display: inline;
}
.invite-button{
    width: 12em;
    height: 3em;
    font-size: 20px;
    background-color:rgb(68, 212, 104);
    color:white;
    border-radius: 2em;
}

.nextgeneration{
    height: 40em;
    display: flex;
    justify-content: space-betweenss;
    background-color: rgb(243, 240, 240);
}

 .nextgeneration .bg-intro{
    width: 57em;
    background-image: url('./images/bg-intro-desktop.svg');
    background-repeat: no-repeat;
    background-position: -3em -13em;
} 
.nextgeneration .bg-intro .mockups{
    width: 50em;
    height: 56em;
    background-image: url('./images/image-mockups.png');
    background-repeat: no-repeat;
    background-position: 8.5em -8em;
    background-size: cover;
    right: 0;
    position: absolute;

}

.nextgeneration .main-content{
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-left: 8em;
    width: 40em;
}

.nextgeneration .main-content .heading-main{
    font-size:2em;
    font-weight: 300;
    color: rgb(74, 74, 136);
}
.nextgeneration .main-content  .content{
    color: gray;
    font-size: 1.5em;
    margin:40px 0;
}
.mobile-mode{
    display: none;
    cursor: pointer;
}

.mobile-menu{
    display: none;
    position: absolute;
    top: 100px;
    background-color: white;
    padding: 10px;
    left: 5%;
    right: 5%;
    height: calc(100vh -50ph);
    width: 85%;
    border-radius: 10px;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
}
.mobile-menu li{
    margin-bottom: 15px;
    padding-top: 10px;
}


@media (max-width:1380px){
    *{
        overflow-x: hidden;
    }
    .desktop-mode{
        display: none;
    }
    .mobile-mode{
        display: block;
    }
    .nextgeneration{
        flex-direction: column;
        height: 100%;
    }
    .bg-intro-mobile{
        width: 100%;
        background-image: url('./images/bg-intro-mobile.svg');
        background-repeat: no-repeat;
        background-size: 100vw;
        background-position: 0 -0;
        margin-bottom: -5em;
    }
    .nextgeneration img{
        width: 100%;
        object-position: 0 -7em;
    }
    .nextgeneration .main-content{
        align-items: center;
        width: 100%;
        padding: 0;

    }
    .nextgeneration .main-content .heading-main{
        padding: 0 .5em;
        font-size: 7vw;
        text-align: center;
    }
    .nextgeneration .main-content .content{
        padding: 0 1em;
        font-size: larger;
        line-height: 1.5em;
        text-align: center;
    }
    .nextgeneration button{
        margin-bottom: 6em;
    }
    .mobile-menu a{
        text-decoration: none;
        color: rgb(146, 137, 137);
    }
}
